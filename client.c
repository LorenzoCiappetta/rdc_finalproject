#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include "utils.h"

size_t message_size_no_padding;
size_t un_len;
char user_name[MAX_USERNAME_SIZE + 1];

void print_error_message(Instruction i, ErrorCode e){
  switch(e){
    case NOTHING_FOUND:
      if(i == LIST){
        printf("\nNo mails in mailbox");
      }
      else{
        printf("\nNo such mail in mailbox\n");
      }
      break;
    case NOT_ALLOWED:
      if(i == LOGIN){
        printf("\nUser is already logged in, program will terminate\n");
      }
      else{
        printf("\nNo such mail addressed to user\n");
      }
      break;
    case WRONG_FORMAT:
      printf("\nMessage did not follow protocol\n");
      break;
    case GENERIC_ERROR:
      if(i == DELETE){
        printf("\nMust read mail before deleting\n");
      }
      break;
  }
}

void read_string_from_terminal(char* buffer, size_t size){
    memset(buffer, 0, BUFFER_SIZE);
    char* s = fgets(buffer, size - 1, stdin);
    if(s == NULL){
      exit_with_error(NULL, "error: read from terminal\n");
    }
    buffer[strlen(buffer) - 1] = '\0';
}

int read_number_from_terminal(int* n){
    int ret = scanf("%d", n);
    if(ret != 1){
      printf("input not recognized, retry\n");
    }
    return ret;
}

char* read_mail_from_file(const char* path){
  int ret;
  ret = open(path, O_DIRECTORY);
  if(-1 != ret){
    close(ret);
    printf("cannot open directory\n");
    return NULL;
  }
  
  int fd = open(path, O_RDONLY);
  if(-1 == fd){
    printf("\nfile not found\n");
    return NULL;
  }
  
  //find how big is file
  size_t file_size = lseek(fd, 0, SEEK_END);
  if(-1 == file_size){
    exit_with_error(NULL,"error: lseek()\n");
  }
  
  ret = lseek(fd, 0, SEEK_SET);
  if(-1 == ret){
    exit_with_error(NULL,"error: lseek()\n");
  }
  
  char* mail = malloc(file_size*sizeof(char));
  if(mail == NULL){
    exit_with_error(NULL, "error: calloc()\n");
  }
  
  ret = read(fd, (void*)mail, file_size*sizeof(char));
  if(-1 == ret){
    exit_with_error(NULL,"error: read()\n");
  }  
  
  ret = close(fd);
  if(-1 == ret){
    exit_with_error(NULL,"error: close()\n");
  }  
  
  return mail;
}

void send_and_receive(int sock, Message* m, size_t size){
  
  int ret = send(sock, (void*)m, message_size_no_padding + size, 0);
  if(ret == -1){
    exit_with_error(NULL, "error: send()\n");
  }
  
  memset(m, 0, sizeof(Message));
  
  ret = recv(sock, (void*)m, message_size_no_padding, 0);
  if(ret == -1){
    exit_with_error(NULL, "error: recv()\n");
  }
  
}

//returns 1 if is opportune closing the connection
int elaborate_response(Message* m, Instruction i){
  if(m -> ins == OK){
    printf("\nOK\n");
  }
  else if(m -> ins = ERROR){
    print_error_message(i, m -> length);
    
    //the username is not valid
    if(i == LOGIN){
      memset(user_name, 0, un_len);
      return 1;
    }

  }
  else{
    free(m);
    exit_with_error(NULL, "server error occurred\n");
  }
  
  return 0;
}

int main(int argc, const char * argv[]){
  //Ctrl+C'ing the client causes the serever to close
  //this circumvents that:
  struct sigaction action = {0};
  action.sa_handler = SIG_IGN;
  sigaction(SIGINT, &action, NULL);
    
  int ret;
  int quit = 0;
  char buffer[BUFFER_SIZE];
  message_size_no_padding = sizeof(Instruction) + sizeof(int) + 2*((MAX_USERNAME_SIZE + 1) * sizeof(char)) + sizeof(char);
  Message* m;
  Message* m_payload;  
  
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if(0 > sock){
    exit_with_error(NULL, "error: socket()\n");
  }
  
  struct sockaddr_in addr = {0};
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htons(INADDR_ANY);
  addr.sin_port = htons(PORT);
  socklen_t addr_len = sizeof(struct sockaddr_in);
  
  ret = connect(sock, (struct sockaddr*)&addr, addr_len);
  if(0 > ret){
      exit_with_error(NULL, "error: connect()\n");
  }  
  
  ret = 0;
  do{
    printf("insert User Name to Login: ");
    read_string_from_terminal(buffer, BUFFER_SIZE);
    
    ret = strlen(buffer) > MAX_USERNAME_SIZE;
    if(ret){
      printf("\nUser Name cannot be longer than %d characters, retry\n\n", MAX_USERNAME_SIZE);
    }
  
  }while(ret);
  
  memcpy(user_name, buffer, strlen(buffer) + 1);
  un_len = strlen(user_name) + 1; 
  
  //sending login request
  m = calloc(1, sizeof(Message));
  if(m == NULL){
    exit_with_error(NULL, "error: calloc()\n");
  }
  
  m -> ins = LOGIN;
  memcpy(m -> recipient, user_name, un_len);
  send_and_receive(sock, m, 0); 
  quit = elaborate_response(m, LOGIN);
  
  //taking orders from client
  while(!quit){
    printf("\nselect action: list, read, send, delete, quit\n");
    
    do{
      read_string_from_terminal(buffer, BUFFER_SIZE);
    }while(buffer[0] == '\0');//this while solves a bug in reading from terminal of unknown source...
    
    //sending list request
    if(!strcmp(buffer, "list")){
    
      memset(m, 0, sizeof(Message));
      m -> ins = LIST;
      memcpy(m -> recipient, user_name, un_len);
      
      ret = send(sock, (void*)m, message_size_no_padding, 0);
      if(ret == -1){
        exit_with_error(NULL, "error: send()\n");
      }
      
      //for each mail, server sends code and sender, client prints them one at a time
      do{
        memset(m, 0, sizeof(Message));
        
        ret = recv(sock, (void*)m, message_size_no_padding, 0);
        if(ret == -1){
          exit_with_error(NULL, "error: recv()\n");
        }
        
        if(m -> ins == ERROR){
          print_error_message(LIST, m -> length);
          break;
        }
        else if(m -> ins == LISTING){
          printf("\nrecieved mail: %d from user: %s", m -> length, m -> sender);
        }
        else if(m -> ins != OK){
          free(m);
          exit_with_error(NULL, "server error occurred\n");
        }
        
      }while(m -> ins != OK);
      
      printf("\n");
    }
    
    //sending read request
    else if(!strcmp(buffer, "read")){
      int mail_id;
      
      do{
        printf("\nselect which mail to read: ");
        ret = read_number_from_terminal(&mail_id);
      }while(ret != 1);
      
      memset(m, 0, sizeof(Message));
      m -> ins = READ;
      m -> length = mail_id;
      memcpy(m -> recipient, user_name, un_len);
      send_and_receive(sock, m, 0);
      
      if(m -> ins == SHOWING){
        int left = m -> length;
        char* mail = malloc(left*sizeof(char));
        
        ret = recv(sock, (void*)mail, left*sizeof(char), 0);
        if(ret == -1){
          exit_with_error(NULL, "error: recv()\n");
        }
    
        printf("\n%s", mail);
        free(mail);
      }
      else if(m -> ins == ERROR){       
        print_error_message(READ, m -> length);
      }
      else{
        free(m);
        exit_with_error(NULL, "server error occurred\n");
      }
      
    }
    
    //sending send request
    else if(!strcmp(buffer, "send")){
      char* mail;
      
      //mail is read from file
      do{
        printf("\nwrite path of mail file\n");
        read_string_from_terminal(buffer, BUFFER_SIZE);
        mail = read_mail_from_file(buffer);
      }while(mail == NULL);
      
      size_t size = strlen(mail) + 1;
      
      m_payload = calloc(1,message_size_no_padding + size);
      if(mail == NULL){
        exit_with_error(NULL, "error: calloc()\n");
      }
      
      m_payload -> ins = SEND;
      m_payload -> length = size;
      memcpy(m_payload -> sender, user_name, un_len);
      
      do{
        printf("\nwrite recipient of mail\n");
        read_string_from_terminal(buffer, BUFFER_SIZE);
        
        ret = strlen(buffer) > MAX_USERNAME_SIZE;
        if(ret){
          printf("\nA user Name cannot be longer than %d characters, retry\n", MAX_USERNAME_SIZE);
        }
        
      }while(ret);
      
      memcpy(m_payload -> recipient, buffer, strlen(buffer) + 1); 
      m_payload -> payload[0] = '$';
      memcpy(m_payload -> payload + 1, mail, size);
      free(mail);
      send_and_receive(sock, m_payload, size);
      elaborate_response(m_payload, SEND);
      free(m_payload);
    }
    
    //sending delete request
    else if(!strcmp(buffer, "delete")){
      int mail_id;
      
      do{
        printf("\nselect which mail to delete: ");
        ret = read_number_from_terminal(&mail_id);
      }while(ret != 1);
    
      memset(m, 0, sizeof(Message));
      m -> ins = DELETE;
      m -> length = mail_id;
      memcpy(m -> recipient, user_name, un_len); 
      send_and_receive(sock, m, 0);
      elaborate_response(m, DELETE);
    }
    
    //sending quit request
    else if(!strcmp(buffer, "quit")){
      quit = 1;//the actual sending is done at end of cycle, like login is done at begin
    }
    
    //user wrote something wrong
    else{
      printf("\ncommand not recognised, try again\n");
    }
  }
  
  //when the cycle stops we need to close the connection
  memset(m, 0, sizeof(Message));    
  m -> ins = QUIT;
  memcpy(m -> recipient, user_name, un_len);
  send_and_receive(sock, m, 0);    
  elaborate_response(m, QUIT);
    
  printf("goodbye\n");
  free(m);
  close(sock);
}
