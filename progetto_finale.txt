Server Casella Postale

-il server mantiene una lista/database della posta contenuta
-il server accetta connessioni da più client
-il server deve essere in grado di gestire accesso condiviso al mail-database
-il server deve rispondere alle richieste dei client
-il server deve inviare messaggi di errore ai client per richieste sbagliate:
	*messaggio non rispetta il protocollo
	*client chiede di leggere una mail non nel database
	*client chiede di leggere una mail non a lui rivolta

-ogni client ha un nome
-ogni client pùo fare le seguenti richieste al server:
	*leggere quali e qunte mail sono a lui rivolte 
	*leggere una certa mail a lui rivolta tra quelle listate
	*inviare un'altra mail ad un altro client
	*eliminare una mail a lui rivolta
	*chiudere la connessione
	
-ogni mail ha mittente, destinatario, codice riconoscitivo
-ogni entry del database ha un codice univoco, la mail associata, il mittente

