//all defines, all miscellaneous function, etc
#define QUEUE_SIZE 5
#define PORT 9999

#define MAX_USERNAME_SIZE 15
#define BUFFER_SIZE 256

typedef enum {LOGIN, LIST, READ, SEND, DELETE, QUIT, LISTING, SHOWING, OK, ERROR} Instruction;
typedef enum {QUIT_CODE = -1, NOTHING_FOUND = -2, NOT_ALLOWED = -3, WRONG_FORMAT = -4, GENERIC_ERROR = -5} ErrorCode;

typedef struct Message{
  Instruction ins;
  int length; //also used for mail code or error code
  char sender[MAX_USERNAME_SIZE + 1];
  char recipient[MAX_USERNAME_SIZE + 1];
  char payload[1];
}Message;

void exit_with_error(int* err, const char * msg);

//not used
int hash_to_code(const char* mail, const char* sender, const char* recipient);
