## MailBox Server

### Description

- the server keeps an in RAM database to store mails.
- In case server is shut down it will save database on files.
- The server can accept more connections from different clients at the same time (multithread).
- The server can handle concurrent access to mail database.
- The server will send error messages in case of bad requests such as:
	* messagge won't follow protocol
	* client requests to read a mail not present in database
	* client requests to read a mail not addressed to them
	* client tries to login with username already used by other client

- Each client is identified by user's username.
- Each client is alloweed the following requests:
	* list which and how many mails are addressed to them 
	* read one of their mails
	* send a mail to the server
	* delete one of their mails
	* quit session, close connection
	
- Each mail has: sender, recipient, identification code.
- Each database entry has a code, a mail, with the same code, sender of the mail.

### Server in detail
The Sever connects on port 9999.

To run, the server requires the path of the directory where database is to be stored. When the server is run it will load all file in RAM database and empty the directory, it is better not to meddle with directory's content.

The server will print on terminal database status and logged users every time it performs an action. 

RAM database is a doubly linked-list type structure with integraded mechanism for process synchronization. Reader/Writer paradigm is already implemented in function that access the database.

### Client in detail

Upon starting, client will ask username to login. In case username is already used by another client it will automatically shutdown.
After that, the client can be given one of five instruction by user as described above. 
* **send** will need user to type the path of the mail file to send and username of recipient.
* **read** and **delete** will need user to type number of desired mail, which can be seen via list command. User cannot delete mail they haven't read.
* **quit** and **list** won't require any other actions by client.

All client interactions with user will occur through terminal.

### Protocol in detail

All messages between server an client use the same struct Message. Message struct is as follows:

```c
typedef struct Message{
	Instruction ins;
	int length; 
	char sender[MAX_USERNAME_SIZE + 1];
	char recipient[MAX_USERNAME_SIZE + 1];
	char payload[1];
}Message;
```
* **ins**, used to store the instruction to send. LOGIN, LIST, READ, SEND, DELETE, QUIT from client and LISTING, SHOWING, OK, ERROR from server. 
* **length**, used for payload length or for error code in case of ERROR instruction. 
* **sender** and **recipient**, arrays of fixed length used store username of sender and recipient.
* **payload**, used to store the , must be allocated dinamically to the size of mail. Only SEND and READ messages need payload.

#### Possible Interactions

- When clients sends LOGIN request: 
	* **ins** = LOGIN.
	* **recipient** = user's username. 
	* If request is valid server will respond **ins** = OK else **ins** = ERROR, **length** = ErrorCode.

- When client sends LIST request: 
	* **ins** = LIST.
	* **recipient** = username. 
	* If request is valid server will respond **ins** = LISTING, **length** = mail code, **recipient** = username, **sender** = mail sender for every valid mail else **ins** = ERROR, **length** = ErrorCode.

- When client sends READ request:
	* **ins** = READ.
	* **recipient** = username.
	* **length** = mail code. 
	* If request is valid server will respond **ins** = SHOWING, **length** = length of mail, **payload** = mail else **ins** = ERROR, **length** = ErrorCode.

- When client sends SEND request:
	* **ins** = SEND.
	* **recipient** = recipient.
	* **sender** = username. 
	* **length** = length of mail.
	* **payload** = mail.
	* If request is valid server will respond **ins** = OK else **ins** = ERROR, **length** = ErrorCode.

- When client sends DELETE request:
	* **ins** = DELETE.
	* **length** = mail code.
	* **recipient** = username. 
	* If request is valid server will respond **ins** = OK else **ins** = ERROR, **length** = ErrorCode.

- When client sends QUIT request:
	* **ins** = QUIT.
	* **recipient** = username. 
	* If request is valid server will respond **ins** = OK else **ins** = ERROR, **length** = ErrorCode.

If client receives message in wrong format it will assume the server is compromised and it will shut down.

If server receives message in wrong format it will send error message to client.
